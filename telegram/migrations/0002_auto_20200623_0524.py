# Generated by Django 3.0.7 on 2020-06-23 05:24

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('telegram', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Countries',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('docs', models.FileField(upload_to='')),
            ],
        ),
        migrations.AddField(
            model_name='telegramuser',
            name='full_username',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='telegramuser',
            name='full_name',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='telegramuser',
            name='step',
            field=models.CharField(choices=[('main_page', 'Main page'), ('faqs', 'FAQs'), ('about', 'About'), ('countries_list', 'Counties List'), ('information', 'Information')], default='main_page', max_length=255),
        ),
        migrations.CreateModel(
            name='Application',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('visa_type', models.CharField(choices=[('student_visa', 'Student visa'), ('tourist_visa', 'Tourist visa'), ('work_visa', 'Work visa'), ('business_visa', 'Business visa'), ('guest_visa', 'Guest visa'), ('pr_visa', 'PR visa')], default='student_visa', max_length=255)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='telegram.TelegramUser')),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='telegram.Countries')),
            ],
        ),
    ]
